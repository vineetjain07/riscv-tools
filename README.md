riscv-tools for shakti
======================

**The main idea of maintaining this repository is to support Shakti classes of processors. This repository hosts the source code of the frequently used risc-v tools**


Individual sources for different tools are hosted as submodules here. Therefore, we have upgraded the versions of each submodule here to the latest.
On need basis, we might pull the latest for each submodule from
[riscv](https://github.com/riscv). The raw sources for spike, compilers, and openocd rests here.

note: The original [riscv-tools](https://github.com/riscv/riscv-tools) repository are not updated.

prerequisites
=============

$ sudo apt-get install autoconf automake autotools-dev curl libmpc-dev libmpfr-dev libgmp-dev libusb-1.0-0-dev gawk build-essential bison flex texinfo gperf libtool patchutils bc zlib1g-dev device-tree-compiler pkg-config libexpat-dev

_Note:_ This requires a compiler with C++11 support (e.g. GCC >= 4.8).
To use a compiler different than the default, use:


Installation steps
==================

Run the commands one by one

mkdir ~/tools

git clone "https://gitlab.com/shaktiproject/software/riscv-tools.git"

cd riscv-tools

git submodule update --init --recursive

export RISCV = < give the path to install riscv toolchain >

./build.sh

export PATH=$PATH:$RISCV/bin:$RISCV/riscv32/bin:$RISCV/riscv64/bin

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$RISCV/riscv32/lib:$RISCV/riscv64/lib

export PATH=$PATH:$RISCV/riscv32/riscv32-unknown-elf/bin

export PATH=$PATH:$RISCV/riscv32/riscv32-unknown-elf/lib

export PATH=$PATH:$RISCV/riscv64/riscv64-unknown-elf/bin

export PATH=$PATH:$RISCV/riscv64/riscv64-unknown-elf/lib


# Queries    
=========

Incase of any queries, raise an [issue](https://gitlab.com/shaktiproject/software/riscv-tools/issues)
